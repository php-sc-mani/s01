<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Reading List Activity</title>
</head>
<body>

	<h1> Reading List Activity </h1>
	<hr>
	<h2>Variables</h2>
	<p><?php echo $firstName ?></p>
	<p><?php echo $lastName ?></p>
	<p><?php echo $age ?></p>
	<p><?php echo $address ?></p>
	<hr>
	<h2>Peso Exchange</h2>
	<p><?php echo pesoExchange(1) ?></p>
	<p><?php echo pesoExchange(5) ?></p>
	<hr>
	<h2>Department List</h2>
	<p><?php echo departmentList(5) ?></p>
	<p><?php echo departmentList(2) ?></p>
	<p><?php echo departmentList(4) ?></p>
	<p><?php echo departmentList(7) ?></p>
	<p><?php echo departmentList(9) ?></p>
	<p><?php echo departmentList(10) ?></p>



</body>
</html>